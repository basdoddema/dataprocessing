import os
from snakemake.remote.NCBI import RemoteProvider as NCBIRemoteProvider
NCBI = NCBIRemoteProvider(email="b.g.doddema@st.hanze.nl") # email required by NCBI

query = '"Zika virus"[Organism] AND (("9000"[SLEN] : "20000"[SLEN]) AND ("2017/03/20"[PDAT] : "2017/03/24"[PDAT])) '
accessions = NCBI.search(query, retmax=4)

input_files = expand("{acc}.fasta", acc=accessions)

rule all:
    input:
        "sizes.fasta"

rule download_and_count:
    input:
        NCBI.remote(input_files, db="nuccore", seq_start=5000)

    output:
        "sizes.fasta"
    benchmark:
        "benchmarks/benchmark.txt"
    run:
        shell("cat {input} > sizes.fasta")
