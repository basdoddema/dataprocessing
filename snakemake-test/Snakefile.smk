SAMPLES = ['Sample1', 'Sample2', 'Sample3']

rule all:
    input:
         'test.txt'

rule quantify_genes:
    input:
         genome='genome.fa',
         r1='data/{sample}.R1.fastq.gz',
         r2='data/{sample}.R2.fastq.gz'
    output:
          '{sample}.txt'
    shell:
         'echo {input.genome} {input.r1} {input.r2} > {output}'

rule samples:
    input:
         expand('{sample}.txt', sample=SAMPLES),
         r1='{sample}.txt',
         r2='{sample}.txt',
         r3='{sample}.txt'
    output:
          'test.txt'
    shell:
         'echo {input.r1} > {output}'

rule clean:
    shell:
         'rm *.txt'
